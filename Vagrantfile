# -*- mode: ruby -*-

# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.define "master" do |master|

    master.vm.box = "centos/6"

    master.vm.box_check_update = false

    master.vm.network "private_network", ip: "192.168.10.10"

    master.vm.network "private_network", ip: "192.168.42.10"

    master.vm.hostname = "master"

    master.vm.synced_folder ".", "/opt/vagrant", type: "rsync"

    master.vm.provider "virtualbox" do |vb|

      vb.gui = false

      vb.memory = "512"

    end

    master.vm.provision "shell", inline: <<-SHELL

      sudo su

      yum -y install epel-release

      yum -y install ipvsadm

      yum -y install keepalived

      cp /etc/keepalived/keepalived.conf /etc/keepalived/keepalived.conf.orig

      cp /opt/vagrant/confs/master/Virtual_IP /etc/sysconfig/network-scripts/ifcfg-eth2:0

      ifup eth2:0

      service network restart

      chkconfig keepalived on

      chkconfig ipvsadm on

      service keepalived start

      service ipvsadm start

      /opt/vagrant/scripts/master/lvsconf.sh

    SHELL

  end

  config.vm.define "nginx1" do |nginx1|

    nginx1.vm.box = "centos/6"

    nginx1.vm.box_check_update = false

    nginx1.vm.network "private_network", ip: "192.168.42.20"

    nginx1.vm.hostname = "nginx1"

    nginx1.vm.synced_folder ".", "/opt/vagrant", type: "rsync"

    nginx1.vm.provider "virtualbox" do |vb|

      vb.gui = false

      vb.memory = "512"

    end

    nginx1.vm.provision "shell", inline: <<-SHELL

      sudo su

      yum -y install epel-release

      yum -y install keepalived

      yum -y install nginx

      mv /etc/sysctl.conf /etc/sysctl.conf.orig

      cp /opt/vagrant/confs/slaves/sysctl.conf /etc/

      cp /opt/vagrant/confs/slaves/Virtual_IP_Nginx1 /etc/sysconfig/network-scripts/ifcfg-lo:0

      ifup lo:0

      service network restart

      sysctl -p /etc/sysctl.conf && ifup lo:0

      sed -i.orig 's/EPEL/nginx1/g' /usr/share/nginx/html/index.html

      chkconfig nginx on

      chkconfig keepalived on

      service nginx start

      service keepalived start

    SHELL

  end

  config.vm.define "nginx2" do |nginx2|

    nginx2.vm.box = "centos/6"

    nginx2.vm.box_check_update = false

    nginx2.vm.network "private_network", ip: "192.168.42.30"

    nginx2.vm.hostname = "nginx2"

    nginx2.vm.synced_folder ".", "/opt/vagrant", type: "rsync"

    nginx2.vm.provider "virtualbox" do |vb|

      vb.gui = false

      vb.memory = "512"

    end

    nginx2.vm.provision "shell", inline: <<-SHELL

      sudo su

      yum -y install epel-release

      yum -y install keepalived

      yum -y install nginx

      mv /etc/sysctl.conf /etc/sysctl.conf.orig

      cp /opt/vagrant/confs/slaves/sysctl.conf /etc/

      cp /opt/vagrant/confs/slaves/Virtual_IP_Nginx2 /etc/sysconfig/network-scripts/ifcfg-lo:0

      ifup lo:0

      service network restart

      sysctl -p /etc/sysctl.conf && ifup lo:0

      sed -i.orig 's/EPEL/nginx2/g' /usr/share/nginx/html/index.html

      chkconfig nginx on

      chkconfig keepalived on

      service nginx start

      service keepalived start

    SHELL

  end

end
