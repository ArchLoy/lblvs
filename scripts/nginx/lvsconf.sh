#!/bin/sh

#on ne forward pas les paquets sur le réseau:
echo "0" >/proc/sys/net/ipv4/ip_forward

#Vérifions si nous pouvons pinger l'adresse de routage:
ping -c 1 192.168.42.51

#et voyons si on arrive à pinger l'adresse virtuelle
#de DIRECTOR:
ping -c 1 192.168.0.51

#si oui, on défini que le routeur par défaut est DIRECTOR:
route add default gw 192.168.0.51
